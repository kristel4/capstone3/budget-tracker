import React, { useContext } from "react";
// Import nextJS Link component for client-side navigation
import Link from "next/link";
import Image from "next/image";
// Import necessary bootstrap components
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { UserContext } from "../UserContext";

export default function NavBar() {
  // Consume the UserContext and destructure it to access the user state from the context provider
  const { user } = useContext(UserContext);

  const profile = user.id ? (
    <>
      <img
        width="35"
        height="35"
        style={{ borderRadius: "100px", marginRight: "10px" }}
        src={user.avatar}
      />
      <Link href="/profile">
        <a className="nav-link text-white" role="button">
          {user.firstName}
        </a>
      </Link>
    </>
  ) : null;

  return (
    <Navbar expand="lg" className="navbarDesign">
      <Link href="/">
        <a className="navbar-brand text-white" id="navTitle">
          The Budgetarian
        </a>
      </Link>
      <Navbar.Toggle aria-controls="justify-content-end" />
      <Navbar.Collapse className="justify-content-end">
        <Nav className="navbarText">
          {user.id !== null ? (
            <React.Fragment>
              {profile}
              <Link href="/logout">
                <a className="nav-link text-white" role="button">
                  Logout
                </a>
              </Link>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Link href="/">
                <a className="nav-link text-white" role="button">
                  Home
                </a>
              </Link>
              <Link href="/login">
                <a className="nav-link text-white" role="button">
                  Login
                </a>
              </Link>
              <Link href="/register">
                <a className="nav-link text-white" role="button">
                  Register
                </a>
              </Link>
            </React.Fragment>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
