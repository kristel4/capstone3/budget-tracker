import React, { useContext, useEffect } from "react";
import { Nav, NavLink, Col } from "react-bootstrap";
import {
  Person,
  BookmarkPlus,
  JournalPlus,
  CashStack,
  Wallet2,
  DashCircle,
  GraphUp,
  PieChart,
} from "react-bootstrap-icons";
// import Icon from "@material-ui/core/Icon";
import { UserContext } from "../UserContext";

const SideBar = ({ props }) => {
  const { user } = useContext(UserContext);

  // const bookmark = window["BookmarkPlus"];
  return !user.id ? null : (
    <Col xs={2} id="sidebar-wrapper">
      <Nav className="col-md-12 d-md-block sidebar m-0 p-0">
        <div className="sidebar-sticky"></div>
        <Nav.Item className="darkSideBorder">
          <Nav.Link href="/profile" className="darkSideText sideBarCenterText">
            <Person size={30} />
            {/* <Icon style={{color: '#a626a1'}}>{  }</Icon> */}
            <span className="sideBarRemovedText">
              {"  "}
              Home
            </span>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="darkSideBorder">
          <Nav.Link
            href="/categories"
            className="darkSideText sideBarCenterText"
          >
            <BookmarkPlus size={30} />
            {/* <Icon style={{color: '#a626a1'}}>{catIcon}</Icon> */}
            <span className="sideBarRemovedText">
              {"  "}
              Categories
            </span>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="darkSideBorder">
          <Nav.Link href="/records" className="darkSideText sideBarCenterText">
            <JournalPlus size={30} />
            <span className="sideBarRemovedText">
              {"  "}
              Records
            </span>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="darkSideBorder">
          <Nav.Link
            href="/monthly-income"
            className="darkSideText sideBarCenterText"
          >
            <CashStack size={30} />
            <span className="sideBarRemovedText">
              {"  "}
              Monthly Income
            </span>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="darkSideBorder">
          <Nav.Link
            href="/monthly-savings"
            className="darkSideText sideBarCenterText"
          >
            <Wallet2 size={30} />
            <span className="sideBarRemovedText">
              {"  "}
              Monthly Savings
            </span>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="darkSideBorder">
          <Nav.Link
            href="/monthly-expense"
            className="darkSideText sideBarCenterText"
          >
            <DashCircle size={30} />
            <span className="sideBarRemovedText">
              {"  "}
              Monthly Expense
            </span>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="darkSideBorder">
          <Nav.Link
            href="/balance-trend"
            className="darkSideText sideBarCenterText"
          >
            <GraphUp size={30} />
            <span className="sideBarRemovedText">
              {"  "}
              Trend
            </span>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="darkSideBorder">
          <Nav.Link
            href="/breakdown"
            className="darkSideText sideBarCenterText"
          >
            <PieChart size={30} />
            <span className="sideBarRemovedText">
              {"  "}
              Breakdown
            </span>
          </Nav.Link>
        </Nav.Item>
      </Nav>
    </Col>
  );
};
export default SideBar;
