import React, { useState, useEffect, useContext } from "react";
import Head from "next/head";
import { RecordContext } from "../../RecordContext";
import { Line } from "react-chartjs-2";
import { Col, Container } from "react-bootstrap";
import moment from "moment";

export default function index() {
  const { records } = useContext(RecordContext);
  const [months, setMonths] = useState([
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ]);
  const [monthlyExpense, setmonthlyExpense] = useState([]);
  useEffect(() => {
    if (records.length) {
      setmonthlyExpense(
        months.map((month) => {
          let expense = 0;
          records.forEach((record) => {
            //moment syntax = moment(date).format(format)
            // console.log(moment(element.sale_date).format("MMMM"));
            if (
              moment(record.dateCreated).format("MMMM") === month &&
              record.categoryType === "Expense"
            ) {
              expense += parseInt(record.amount);
            }
          });
          return expense;
        })
      );
    }
  }, [records]);

  const data = {
    labels: months,
    datasets: [
      {
        label: "Monthly Expense",
        backgroundColor: "rgba(232, 150, 0, 0.55)",
        borderColor: "#fcdc64",
        borderWidth: 1,
        fill: true,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        data: monthlyExpense,
      },
    ],
  };

  return (
    <React.Fragment>
      <Col xs={10} id="page-content-wrapper">
        <h1 className="text-center darkTextColor m-2">Monthly Expense</h1>
        <Container>
          <Line data={data} />
        </Container>
      </Col>
    </React.Fragment>
  );
}
