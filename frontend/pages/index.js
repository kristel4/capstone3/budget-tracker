import Link from "next/link";
import Image from "next/image";
import { Row, Col, Jumbotron, Container, Button } from "react-bootstrap";

export default function home({ data }) {
  return (
    <Container fluid className="mt-3">
      <Row>
        <Col>
          <Jumbotron
            className="m-0 p-3"
            style={{ boxShadow: "0 0 10px #7b9481" }}
          >
            <h1 className="darkTextColor">Welcome Budgetarian!</h1>
            <div className="text-center">
              <h2>Be disciplined!</h2>
              <h2>
                Put your <span className="savingsText">savings</span> first!
              </h2>
              <h6 className="text-muted">Always remember this rule:</h6>
              <span>
                <Image src="/cover.png" alt="income" width="600" height="200" />
              </span>
              <p className="text-muted mt-4">
                This web application will help you get on track with your
                financial goals and make a better life for yourself and for your
                loveones.
              </p>
              <h6 className="darkTextColor">
                Are you ready for a better future?
              </h6>
              <Button id="darkButton">
                <Link href="/login">
                  <a style={{ textDecoration: "none", color: "#ddffbc" }}>
                    Get Started
                  </a>
                </Link>
              </Button>
            </div>
          </Jumbotron>
        </Col>
      </Row>
    </Container>
  );
}
