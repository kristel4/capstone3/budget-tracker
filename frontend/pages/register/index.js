import { useState, useEffect } from "react";
import Router from "next/router";
import Link from "next/link";
import { Form, Button, Card, Row, Col, Container } from "react-bootstrap";
import Swal from "sweetalert2";
import View from "../../components/View";
import AppHelper from "../../app-helper";

export default function index() {
  return (
    <Col xs={12} id="page-content-wrapper">
      <View title={"Register"}>
        <Row className="justify-content-center">
          <Col xs md="8">
            <RegisterForm />
          </Col>
        </Row>
      </View>
    </Col>
  );
}

const RegisterForm = () => {
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState(0);
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    fetch(`${AppHelper.API_URL}/users/email-exists`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === false) {
          fetch(`${AppHelper.API_URL}/users`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              mobileNo: mobileNo,
              email: email,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data) {
                setEmail("");
                setFirstName("");
                setLastName("");
                setMobileNo(0);
                setPassword1("");
                setPassword2("");

                Swal.fire({
                  position: "center",
                  icon: "success",
                  title: "Thank you for registering with us!",
                  showConfirmButton: false,
                  timer: 1500,
                });

                Router.push("/login");
              }
            });
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Email already exists!",
            footer: "Please provide another email.",
          });
        }
      });
  }

  useEffect(() => {
    if (
      email !== "" &&
      firstName !== "" &&
      lastName !== "" &&
      mobileNo.length === 11 &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, firstName, lastName, mobileNo, password1, password2]);

  return (
    <Card style={{ backgroundColor: "#ddffbc", boxShadow: "0 0 10px #7b9481" }}>
      <Card.Body>
        <h2 className="p-4 text-center darkTextColor darkTextBorder">
          REGISTER
        </h2>
        <p className="text-center text-muted darkTextColor">
          Create your account. It's free and only takes a minute.
        </p>
        <Container>
          <Form onSubmit={registerUser}>
            <Form.Group as={Row} controlId="userEmail" className="m-0 pb-0">
              <Form.Label column xs={12} md={3} className="text-left">
                Email:
              </Form.Label>
              <Col xs={12} md={9} className="p-0 m-auto">
                <Form.Control
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="Email Address"
                  required
                />
              </Col>
              <Col xs={12} className="p-0 mt-0 mx-auto">
                <Form.Text className="text-muted text-center m-0 pb-1">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="firstName" className="m-0  pb-3">
              <Form.Label column xs={12} md={3} className="text-left">
                First Name:
              </Form.Label>
              <Col xs={12} md={9} className="p-0 m-auto">
                <Form.Control
                  type="text"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                  placeholder="First Name"
                  required
                />
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="lastName" className="m-0 pb-3">
              <Form.Label column xs={12} md={3} className="text-left">
                Last Name:
              </Form.Label>
              <Col xs={12} md={9} className="p-0 m-auto">
                <Form.Control
                  type="text"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  placeholder="Last Name"
                  required
                />
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="mobileNo" className="m-0 pb-3">
              <Form.Label column xs={12} md={3} className="text-left">
                Mobile Number:
              </Form.Label>
              <Col xs={12} md={9} className="p-0 m-auto">
                <Form.Control
                  type="number"
                  value={mobileNo}
                  onChange={(e) => setMobileNo(e.target.value)}
                  required
                />
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="password1" className="m-0 pb-3">
              <Form.Label column xs={12} md={3} className="text-left">
                Password:
              </Form.Label>
              <Col xs={12} md={9} className="p-0 m-auto">
                <Form.Control
                  type="password"
                  value={password1}
                  onChange={(e) => setPassword1(e.target.value)}
                  placeholder="Password"
                  required
                />
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="password2" className="m-0 pb-3">
              <Form.Label column xs={12} md={3} className="text-left">
                Verify Password:
              </Form.Label>
              <Col xs={12} md={9} className="p-0 m-auto">
                <Form.Control
                  type="password"
                  value={password2}
                  onChange={(e) => setPassword2(e.target.value)}
                  placeholder="Verify Password"
                  required
                />
              </Col>
            </Form.Group>

            {isActive ? (
              <Button type="submit" block className="mt-3" id="darkButton">
                Register
              </Button>
            ) : (
              <Button block disabled className="mt-3" id="darkButton">
                Fill out the form to register.
              </Button>
            )}

            <Form.Group as={Row} className="mb-0">
              <Form.Label column lg={12} className="text-center">
                <p className="text-center darkTextColor">
                  Already have an account?{" "}
                  <Link href="/login">
                    <strong>Sign in here.</strong>
                  </Link>
                </p>
              </Form.Label>
            </Form.Group>
          </Form>
        </Container>
      </Card.Body>
    </Card>
  );
};
